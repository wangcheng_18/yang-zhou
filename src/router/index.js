import Vue from 'vue'
import Router from 'vue-router'
import Index from '@/pages/Index'
import Jade from '@/pages/Jade'

import YuQi from '@/components/index/YuQi'
import TongQi from '@/components/index/TongQi'
import JinYin from '@/components/index/JinYin'
import TaoCi from '@/components/index/TaoCi'
import QiMu from '@/components/index/QiMu'
import ZaQi from '@/components/index/ZaQi'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      redirect: '/index'
    },
    {
      path: '/index',
      name: 'Index',
      component: Index,
      redirect: '/index/yuqi',
      children:[
        {
          path:'yuqi',
          name:'YuQi',
          component:YuQi
        },
        {
          path:'tongqi',
          name:'TongQi',
          component:TongQi
        },
        {
          path:'jinyin',
          name:'JinYin',
          component:JinYin
        },
        {
          path:'taoci',
          name:'TaoCi',
          component:TaoCi
        },
        {
          path:'qimu',
          name:'QiMu',
          component:QiMu
        },
        {
          path:'zaqi',
          name:'ZaQi',
          component:ZaQi
        }
      ]
    },
    {
      path: '/jade',
      name: 'Jade',
      component: Jade
    }
  ]
})
