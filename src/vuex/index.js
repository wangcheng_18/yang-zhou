import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

const store =new Vuex.Store({
    state:{
        hotspot:""
    },
    mutations:{
        CHANGETYPE(state,type){
            state.hotspot=type
            // console.log(state.hotspot)
        }
    },
    actions:{
        changeType({ commit },type){
            commit('CHANGETYPE', type)
        }
    },
    getters:{
        hotspot: state => state.hotspot,
    }
})

export default store